<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'org\\bovigo\\vfs' => array($vendorDir . '/mikey179/vfsStream/src/main/php'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Composer\\Installers\\' => array($vendorDir . '/composer/installers/src'),
);
