<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sessions extends CI_Controller {
    public function create()
    {
        $error = null;
        if ($this->input->method(TRUE) == 'POST')
        {
            $credentials = $this->input->post('user');
            if ($credentials)
            {
                // ... try to autenticate the user
                try {
                    $this->user = Sentinel::authenticate($credentials);
                    if ($this->user)
                    {
                        $error = null;
                        redirect('/');
                    }
                    else
                    {
                        $error = (object) ['message' => 'Cannot authenticate user, please try again'];
                    }
                } catch (Exception $e){
                    $reflection = new ReflectionClass($e);
                    $property = $reflection->getProperty('message');
                    $property->setAccessible(true);
                    $error = (object) ['message' => $property->getValue($e)];
                }
            }
        }

        // .. render the view
        $this->load->blade('sessions.create', array('error' => $error));
    }

    public function destroy()
    {

    }
}