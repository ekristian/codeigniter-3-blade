<?php

// Import the necessary classes
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Capsule\Manager as Capsule;


class MY_Sentinel
{
    public function __construct(){
        // Setup a new Eloquent Capsule instance
		$capsule = new Capsule;

		$capsule->addConnection([
		    'driver'    => 'mysql',
		    'host'      => 'localhost',
		    'database'  => 'development_db',
		    'username'  => 'root',
		    'password'  => 'root',
		    'charset'   => 'utf8',
		    'collation' => 'utf8_unicode_ci',
		]);

		$capsule->bootEloquent();
    }
}